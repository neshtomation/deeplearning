# deepLearning

## 1 Churn Modelling Problem
Build an ANN for bank to solve customer attrition problem.

In this project we are developing a deep learning model by making an Artificial Neural Networks to solve a Customer leaving the bank problem.

We will have a dataset with large sample of the Bank's customers. To make this dataset, the bank gathered information such as customer id, credit score, gender, age, tenure, balance, if the customer is active, has a credit card, etc. DUring period of 6 months, the bank observed if these customers left or stayed in the bank.

Our goal here is to make an ANN that can predict, based on geo-demographical and transactional information given above, if any individual customer will leave the bank or stay (customer attrition). Besides, you are asked to rank all the customers of the bank, based on their probability of leaving. To do that, you will need to use the right Deep Learning model, one that is based on a probabilistic approach.

## 2 Image Recognition

Convolutional Neural Networks for Image Recognition
In this part, we will create a Convolutional Neural Network that is able to detect various objects in images. We will implement this Deep Learning model to recognize a cat or a dog in a set of pictures. However, this model can be reused to detect anything else and we will show you how to do it - by simply changing the pictures in the input folder. 

For example, we will be able to train the same model on a set of brain images, to detect if they contain a tumor or not. But if you want to keep it fitted to cats and dogs, then you will literally be able to a take a picture of your cat or your dog, and your model will predict which pet you have. We even tested it out on Hadelin’s dog!
